### Links to Topics, Demos and Discussions:

**November 2019**

- Learn and discuss APIs - [Recording](https://sms.cam.ac.uk/media/3114614)

**October 2019**

- Automating DPK installs with GitLab and Ansible - Notes (soon)

**September 2019**

- Learn and discuss automation in operations - [Notes](https://intranet.uis.cam.ac.uk/downloads/technology-cop-learn-and-discuss-automation-in-operations) and [recording](https://sms.cam.ac.uk/media/3065890)


**July 2019**

- Technical debt and code review - [Notes](Notes)
- Automated front end testing, using github to manage local code changes, GitLab Google Cloud demo - [Notes](Notes)
- Long range wide area network, Agile management of projects - [Notes](Notes)
- APIs, traditional SysAdmin merging with automation, CI/CD, definitive data sources - [Notes](Notes)
- Learn and discuss automation in operations 