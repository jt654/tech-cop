# Welcome to the Technology Community

### A way of sharing knowledge, best practices and learn from others

The aim of the technology community of practice is to: 

- Connect people who might not otherwise have the opportunity to interact
- Provide a shared context for people to communicate and share information, stories, and personal experiences
- Enable dialogue between people who come together to explore new possibilities, solve challenging problems, and create new, mutually beneficial opportunities
- Stimulate learning by serving as a vehicle for authentic communication, mentoring, coaching, and self-reflection 
- Capture and diffuse existing knowledge to help people improve their practice by providing a channel to identify solutions to common problems and a process to collect and evaluate best practices
- Introduce collaborative processes to groups to encourage the free flow of ideas and exchange of information
- Help people organise around purposeful actions that deliver tangible results
- Generate new knowledge to help people transform their practice to accommodate changes in needs and technologies 

![Tech Community](https://youthincmag.com/wp-content/uploads/2018/09/Tech-in-Education.jpg)