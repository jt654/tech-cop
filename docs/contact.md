# Contact

Subscribe to the [Tech CoP email](https://lists.cam.ac.uk/mailman/listinfo/ucam-technology-cop), join discussion on Slack:  [#tech-cop](https://app.slack.com/client/T0EM6990C/CQ7BRTKTM) or contact [Julia Torrejon](mailto:jt654@uis.cam.ac.uk) if you'd like to suggest ideas for future Technology forums and events.


![](https://clm-supplies.com/wp-content/uploads/2019/08/contact-us.png)