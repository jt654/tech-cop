A curated list of helpful links of documentation, guides and other helpful resources.

### APIs
- [How to document APIs](https://www.gov.uk/guidance/how-to-document-apis)

### BrowserStack 
- [Browser Guide](https://www.browserstack.com/guide)

### Docker
- [Docker Documentation](https://docs.docker.com/)

### Git
- [Git Documentation](https://git-scm.com/doc)

### GitLab
- [GitLab Docs](https://docs.gitlab.com/)

### Selenium
- [Selenium Documentation](https://www.selenium.dev/documentation/en/)
